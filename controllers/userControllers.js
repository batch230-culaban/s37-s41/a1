const User = require("../models/user.js");
const Course = require("../models/course.js");
const bcrypt = require("bcrypt");
const auth = require("../auth.js");


module.exports.registerUser = (reqBody) => {
    let newUser = new User ({
        firstName: reqBody.firstName,
        lastName: reqBody.lastName,
        email: reqBody.email,
        password: bcrypt.hashSync(reqBody.password, 10),
        // 10 - salt (crack) no of tries
        mobileNo: reqBody.mobileNo,
    })

    return newUser.save()
    .then((user, error) => 
    {
        if(error){
            return false;
        }else{
            return true;
        }
    })
}

/* function checkEmailExist{}*/

module.exports.checkEmailExists = (reqBody) => {
    return User.find({email: reqBody.email})
    .then(result => {
        if(result.length > 0){
            return true
        }
        else {
            return false
        }
    })
}

module.exports.loginUser = (reqBody) => {
    return User.findOne({email: reqBody.email})
    .then(result => {
        if(result == null){
            return false
        }
        else{
            // compareSync is bcrypt function to compare unhashed password to hashed password
            const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);
            // true or false
            if(isPasswordCorrect){
                // Let's give the user a token to a access features
                return {access: auth.createAccessToken(result)};
            }
            else{
                // If password does not match, else
                return false;
            }
        }
    })
}

// s38 Activity (my Answer)
// module.exports.getProfile = (reqBody) => {
// 	return User.findById(reqBody.id)
//     .then((result, err) => {
// 		if(err){
// 			return false;
// 		}else{
//             result.password = '*****';
//             return result;
//         }
//     })
// }

// to Get all Users to find the specific IDs

module.exports.getAllUser = (req, res) => {
	User.find({})
	.then(result => res.send(result)) // result.name : Specific name kwaon
	.catch(error => res.send(error));
}



// S41 alternate example/solution ni sir
module.exports.getProfile = (request, response) => {

    // getting the Token
    const userData = auth.decode(request.headers.authorization);
    console.log(userData)

    return User.findById(userData.id).then(result =>
        {
            result.password = "*****";
            response.send(result);
        })
}


// Enroll Feature

module.exports.enroll = async (request, response) => {

    const userData = auth.decode(request.headers.authorization);

    let courseName = await Course.findById(request.body.courseId)
    .then(result => result.name);

    let newData = {
        // User ID and email will be retrieved from the rqeuest heeader
        userId: userData.id,
        email: userData.email,
        // course ID will be retrieved from the request body
        courseId: request.body.courseId,
        courseName: courseName
    }
    console.log(newData);

    let isUserUpdated = await User.findById(newData.userId)
    .then(user => {
        user.enrollments.push({
            courseId: newData.courseId,
            courseName: newData.courseName,
        })
    return user.save()
    .then(result => {
        console.log(result);
        return true
    })
    .catch(error => {
        console.log(error);
        return false;
    })
})
    console.log(isUserUpdated);

    let isCourseUpdated = await Course.findById(newData.courseId)
    .then(course => {
        course.enrollees.push({
            userId: newData.userId,
            email : newData.email
        })
        // Mini Activity -
		// [1] Create a condition that if the slots is already zero, no deduction of slot will happen and...
		// [2] it should have a response that a user is not allowed to enroll due to no available slots
		// [3] Else if the slot is negative value, it should make the slot value to zero

    // long Method: course.slots = course.slots - 1;
    if (Course.slots >= 0){
        Course.slots -= 1;
        return course.save()
            .then(result => {
            console.log(result);
            return true
        })
            .catch(error => {
            console.log(error);
            return false;
        })
    }
        // else{
        //     return course.save()
        //     .then(result => {
        //         response.send("No Slots Available, Can't Enroll Anymore");
        //     })
        //     .catch(error => {
        //         console.log(error);
        //         return false;
        //     })
        // }
    })

    console.log(isCourseUpdated);
    // Condition will check if the both "user" and "course" document has been updated
    // Ternary Operator
    (isUserUpdated == true && isCourseUpdated == true && Course.slots != 0)? 
    response.send(true) : response.send("No Slots Available, Can't Enroll Anymore");
    /* Equivalent Meaning for Ternary Operator
    if(isUserUpdated == true && isCourseUpdated == true){
        response.send(true)
    }
    else{
        response.send(false)
    }
    */
    }