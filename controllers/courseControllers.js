const mongoose = require("mongoose");
const Course = require("../models/course.js");

// Function for adding a course
// 2. Update the "addCourse" controller method to implement admin authentication for creating a course
module.exports.addCourse = (reqBody, addCourseByAdmin) => {
    if(addCourseByAdmin.isAdmin == true){
        
    let newCourse = new Course ({
        name: reqBody.name,
        description: reqBody.description,
        price: reqBody.price,
        slots: reqBody.slots
    })

        return newCourse.save() 
        .then((newCourse, error) =>
        {
            if(error){
                return error;
            }
            else{
                return newCourse;
            }
        })
    }
    else{
        let message = Promise.resolve('User must be ADMIN to access this functionality');
		return message.then((value) => {return value});
    }
}

//Get All Course
module.exports.getAllCourse = () =>{
    return Course.find({})
    .then (result =>{
        return result;
    })
}

// Get All Active Courses
module.exports.getActiveCourses = () =>{
    return Course.find({isActive: true})
    .then (result =>{
        return result;
    })
}

// Get Specific Courses
module.exports.getCourse = (courseId) =>{
    return Course.findById(courseId)
    .then (result =>{
        return result;
    })
}

// UPDATING a course
module.exports.updateCourse = (courseId, newData) => {
	if(newData.isAdmin == true){
		return Course.findByIdAndUpdate(courseId,
			{
				// newData.course.name
				// newData.request.body.name
				name: newData.course.name, 
				description: newData.course.description,
				price: newData.course.price,
			}
		).then((result, error)=>{
			if(error){
				return false;
			}
			return true
		})
	}
	else{
		let message = Promise.resolve('User must be ADMIN to access this functionality');
		return message.then((value) => {return value});
	}
}


// s40 Activity (Archiving)
/*
1. Create a route for archiving a course. The route must use JWT authentication and obtain the course ID from the url.
2. Create a controller method for archiving a course obtaining the course ID from the request params and the course information from the request body.
3. Process a PUT request at the /courseId/archive route using postman to archive a course
4. Create a git repository named S35.
5. Add another remote link and push to git with the commit message of Add activity code - S35.
6. Add the link in Boodle.
*/

module.exports.archiveCourse = (courseId, courseArchiveByAdmin) => {
    if (courseArchiveByAdmin.isAdmin == true){
        return Course.findByIdAndUpdate(courseId,
            {
                isActive: courseArchiveByAdmin.course.isActive
            })
            .then((result, error) => {
                if(error){
                    return false;
                }
                return true
            })
    }
    else{
        let message = Promise.resolve('User must be ADMIN to access this functionality');
        return message.then((value) => {return value});
    }
}