const express = require("express");
const router = express.Router();
const courseControllers = require("../controllers/courseControllers.js");
const auth = require("../auth.js");
const { response } = require("express");
const course = require("../models/course.js");

// Activity
// 1. Refractor the "course" route to implement user authentication for the admin when creating a course.


// Creating a Course
router.post("/create", auth.verify, (request, response) => 
{
    const addCourseByAdmin = {
		isAdmin: auth.decode(request.headers.authorization).isAdmin
	}
    courseControllers.addCourse(request.body, addCourseByAdmin)
    .then(resultFromController => response.send(resultFromController))
})

// Get all Courses
router.get("/all", (request, response) => {
    courseControllers.getAllCourse()
    .then(resultFromController => response.send(resultFromController))
})

// Get all Active Courses

router.get("/active", (request, response) => {
    courseControllers.getActiveCourses()
    .then(resultFromController => response.send(resultFromController))
})

// Get Specific Course
router.get("/:courseId", (request, response) => {
    courseControllers.getCourse(request.params.courseId)
    .then(resultFromController => response.send(resultFromController))
})

// Update course
router.patch("/:courseId/update", auth.verify, (request,response) => 
{
	const newData = {
		course: request.body, 	//request.headers.authorization contains jwt
		isAdmin: auth.decode(request.headers.authorization).isAdmin
	}
	courseControllers.updateCourse(request.params.courseId, newData).then(resultFromController => {
		response.send(resultFromController)
	})
})

// S40 Activity
router.patch("/:courseId/archive", auth.verify, (request, response) =>
{
    const courseArchiveByAdmin = {
        course : request.body,
        isAdmin: auth.decode(request.headers.authorization).isAdmin
    }
    courseControllers.archiveCourse(request.params.courseId, courseArchiveByAdmin)
    .then(resultFromController => {
        response.send(resultFromController)
    })
})



module.exports = router;