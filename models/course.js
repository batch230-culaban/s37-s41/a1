// dependecy

const mongoose = require("mongoose");

const courseSchema = new mongoose.Schema({

    name : {
        type: String,
        required: [true, "Course is required"]
    },

    description: {
        type: String,
        required: [true, "Description is required"]
    },

    price: {
        type: Number,
        required : [true, "Price is required"]
    },

    isActive: {
        type: Boolean,
        default: true
    },
    slots: {
        type: Number,
        required: [true, "Slots is required"]
    },

    createdOn: {
        type: Date,
        // The "new Date()" expression instantiates a new "Date" that the current date and time whenever a course is creatred in our Database
        default : new Date()
    },

    enrollees : [
        {
            userId: {
                type : String,
                required: [true, "UserId is required"]
            },
            enrolledOn: {
                type: Date,
                default: new Date()
            }
        }
    ]

});

module.exports = mongoose.model("Course", courseSchema);